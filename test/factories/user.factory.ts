import { Factory } from '@linnify/typeorm-factory';
import { User } from '../../src/business/user/user.entity';
import * as faker from 'faker';

export class UserFactory extends Factory<User> {
  entity = User;

  email: string = faker.internet.email();
  password = 'password';
  fullname: string = faker.name.findName();
}
