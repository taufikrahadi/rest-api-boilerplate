import { ConfigService } from '@nestjs/config';
import { User } from '../../src/business/user/user.entity';
import { sign } from 'jsonwebtoken';

export const login = (user: User): string => {
  const configService = new ConfigService({});

  return sign(
    {
      userId: user.id,
      email: user.email,
      fullname: user.fullname,
    },
    configService.get<string>('JWT_SECRET'),
    { expiresIn: '7d' },
  );
};
