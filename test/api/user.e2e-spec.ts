import { INestApplication } from '@nestjs/common/interfaces';
import { Test } from '@nestjs/testing';
import {
  HttpStatus,
  UnprocessableEntityException,
  ValidationPipe,
} from '@nestjs/common';
import { UserModule } from '../../src/application/api/user/user.module';
import { MainModule } from '../modules/main';
import * as supertest from 'supertest';
import { ValidationError } from 'class-validator';
import { truncate } from '../utils/truncate';
import { UserFactory } from '../factories/user.factory';
import { login } from '../utils/login';

describe('user test', () => {
  let app: INestApplication;
  const userFactory = new UserFactory();

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      imports: [MainModule, UserModule],
      providers: [],
    }).compile();

    app = module.createNestApplication();

    app.useGlobalPipes(
      new ValidationPipe({
        exceptionFactory: (errors: ValidationError[]) => {
          const message = errors.map((error) => {
            const { property, constraints } = error;
            const keys = Object.keys(constraints);

            const msgs: string[] = [];

            keys.forEach((key) => {
              msgs.push(`${constraints[key]}`);
            });

            return {
              property,
              errors: msgs,
            };
          });

          throw new UnprocessableEntityException(message);
        },
      }),
    );

    await app.init();
  });

  beforeEach(async () => {
    await truncate();
  });

  describe('/user', () => {
    describe('/register', () => {
      it('should return created user', async () => {
        const response = await supertest(app.getHttpServer())
          .post('/user/register')
          .send({
            email: 'test@mail.com',
            fullname: 'test',
            password: 'password',
          })
          .expect(HttpStatus.CREATED);

        expect(response.body).toMatchObject({
          email: 'test@mail.com',
          fullname: 'test',
          id: expect.any(String),
          createdAt: expect.any(String),
          updatedAt: expect.any(String),
        });
      });

      it('should return 422 error (email already exists)', async () => {
        const user = await userFactory.create();

        const response = await supertest(app.getHttpServer())
          .post('/user/register')
          .send({
            email: user.email,
            fullname: 'test',
            password: 'password',
          })
          .expect(HttpStatus.UNPROCESSABLE_ENTITY);

        expect(response.body.message[0].errors[0]).toBe(
          `${user.email} already in use`,
        );
      });
    });

    describe('/profile', () => {
      it('should return 401', async () => {
        await supertest(app.getHttpServer())
          .get('/user/profile')
          .expect(HttpStatus.UNAUTHORIZED);
      });

      it('should return authenticated user profile', async () => {
        const user = await userFactory.create();
        const token = login(user);

        const response = await supertest(app.getHttpServer())
          .get('/user/profile')
          .set('Authorization', `Bearer ${token}`)
          .expect(HttpStatus.OK);

        expect(response.body).toMatchObject({
          id: user.id,
          email: user.email,
          fullname: user.fullname,
        });
      });
    });
  });

  afterAll(async () => {
    await app.close();
  });
});
