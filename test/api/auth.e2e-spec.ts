import {
  HttpStatus,
  INestApplication,
  UnprocessableEntityException,
  ValidationPipe,
} from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { ValidationError } from 'class-validator';
import { AuthModule } from '../../src/application/api/auth/auth.module';
import { MainModule } from '../modules/main';
import { truncate } from '../utils/truncate';
import { UserFactory } from '../factories/user.factory';
import * as supertest from 'supertest';

describe('auth test', () => {
  let app: INestApplication;
  const userFactory = new UserFactory();

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      imports: [MainModule, AuthModule],
      providers: [],
    }).compile();

    app = module.createNestApplication();

    app.useGlobalPipes(
      new ValidationPipe({
        exceptionFactory: (errors: ValidationError[]) => {
          const message = errors.map((error) => {
            const { property, constraints } = error;
            const keys = Object.keys(constraints);

            const msgs: string[] = [];

            keys.forEach((key) => {
              msgs.push(`${constraints[key]}`);
            });

            return {
              property,
              errors: msgs,
            };
          });

          throw new UnprocessableEntityException(message);
        },
      }),
    );

    await app.init();
  });

  beforeEach(async () => {
    await truncate();
  });

  describe('/auth', () => {
    it('should return userid, email, and access token', async () => {
      const user = await userFactory.create();

      const response = await supertest(app.getHttpServer())
        .post('/auth/login')
        .send({
          email: user.email,
          password: 'password',
        })
        .expect(HttpStatus.OK);

      expect(response.body).toMatchObject({
        userId: expect.any(String),
        email: expect.any(String),
        accessToken: expect.any(String),
      });
    });

    it('should return 400 (wrong password)', async () => {
      const user = await userFactory.create({
        password: 'examplepass',
      });

      const response = await supertest(app.getHttpServer())
        .post('/auth/login')
        .send({
          email: user.email,
          password: 'password',
        })
        .expect(HttpStatus.BAD_REQUEST);

      expect(response.body.message).toBe('Wrong Password');
    });

    it('should return 422 (user not found)', async () => {
      await userFactory.create();

      const response = await supertest(app.getHttpServer())
        .post('/auth/login')
        .send({
          email: 'mail@mail.com',
          password: 'password',
        })
        .expect(HttpStatus.UNPROCESSABLE_ENTITY);

      expect(response.body.message[0].errors[0]).toBe(
        `data mail@mail.com is not exists`,
      );
    });
  });

  afterAll(async () => {
    await app.close();
  });
});
