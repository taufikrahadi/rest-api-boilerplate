import { ApiProperty } from '@nestjs/swagger';
import {
  CreateDateColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

export class BaseEntity {
  @PrimaryGeneratedColumn('uuid')
  @ApiProperty({
    type: String,
    format: 'uuid',
  })
  id: string;

  @CreateDateColumn()
  @ApiProperty({
    type: String,
    format: 'date-time',
  })
  createdAt?: Date;

  @UpdateDateColumn()
  @ApiProperty({
    type: String,
    format: 'date-time',
  })
  updatedAt?: Date;
}
