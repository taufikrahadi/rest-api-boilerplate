import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { getRepository } from 'typeorm';

export function IsExists(
  key: string,
  repository: any,
  validationOptions?: ValidationOptions,
) {
  return (object: any, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: {
        message: ({ value }) => `data ${value} is not exists`,
        ...validationOptions,
      },
      constraints: [key, repository],
      validator: IsExistsConstraints,
    });
  };
}

@ValidatorConstraint({ name: 'IsExists' })
export class IsExistsConstraints implements ValidatorConstraintInterface {
  async validate(value: any, args: ValidationArguments) {
    const [key, repo] = args.constraints;
    const isUnique = await getRepository(repo)
      .createQueryBuilder('alias')
      .where(`alias.${key} = :id`, { id: value })
      .getOne();

    return Boolean(isUnique);
  }
}
