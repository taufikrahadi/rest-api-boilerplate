import {
  Logger,
  UnprocessableEntityException,
  ValidationPipe,
} from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AppModule } from './application/app.module';
import { ConfigService } from '@nestjs/config';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as morgan from 'morgan';
import { ValidationError } from 'class-validator';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  const logger = new Logger('Main Application');
  const reqLogger = new Logger('Incoming Request');

  app.enableCors({
    origin: '*',
  });

  app.use(
    morgan('combined', {
      stream: {
        write: (message) => reqLogger.verbose(message),
      },
    }),
  );

  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory: (errors: ValidationError[]) => {
        const message = errors.map((error) => {
          const { property, constraints } = error;
          const keys = Object.keys(constraints);

          const msgs: string[] = [];

          keys.forEach((key) => {
            msgs.push(`${constraints[key]}`);
          });

          return {
            property,
            errors: msgs,
          };
        });

        throw new UnprocessableEntityException(message);
      },
    }),
  );

  const swaggerConfig = new DocumentBuilder()
    .setTitle('Dot Hiring')
    .addBearerAuth()
    .build();

  const swaggerDocs = SwaggerModule.createDocument(app, swaggerConfig);

  SwaggerModule.setup('docs', app, swaggerDocs);

  const configService = app.get<ConfigService>(ConfigService);
  const port = configService.get<number>('PORT') ?? 8080;

  app.setGlobalPrefix('api');
  await app.listen(port);
  logger.log(`Application is running on port ${port}`);
}
bootstrap();
