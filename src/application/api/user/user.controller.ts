import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Post,
  UseGuards,
} from '@nestjs/common';
import { ApiOkResponse, ApiSecurity, ApiTags } from '@nestjs/swagger';
import { UserInfo } from '../../common/decorators/userinfo.decorator';
import { JwtAuthGuard } from '../../common/guards/auth.guard';
import { CreateUserDto } from '../../../business/user/dto/create.dto';
import { User } from '../../../business/user/user.entity';
import { UserService } from '../../../business/user/user.service';

@Controller('user')
@ApiTags('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('/hello')
  helloUser() {
    return 'hello user';
  }

  @Post('register')
  @ApiOkResponse({ type: User, status: HttpStatus.CREATED })
  registerUser(@Body() createUserDto: CreateUserDto) {
    return this.userService.createUser(createUserDto);
  }

  @Get('profile')
  @ApiSecurity('jwt')
  @ApiOkResponse({ type: User, status: HttpStatus.OK })
  @UseGuards(JwtAuthGuard)
  userProfile(@UserInfo('id') userId: string) {
    return this.userService.findById(userId);
  }
}
