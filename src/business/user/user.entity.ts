import { ApiProperty } from '@nestjs/swagger';
import { genSaltSync, hash } from 'bcrypt';
import { BaseEntity } from '../../resources/utils/classes/BaseEntity';
import { BeforeInsert, Column, Entity } from 'typeorm';

@Entity('Users')
export class User extends BaseEntity {
  @Column({
    unique: true,
  })
  @ApiProperty()
  email: string;

  @Column()
  @ApiProperty()
  fullname: string;

  @Column({
    select: false,
  })
  password: string;

  @BeforeInsert()
  async hashPassword() {
    this.password = await hash(this.password, genSaltSync(12));
  }
}
