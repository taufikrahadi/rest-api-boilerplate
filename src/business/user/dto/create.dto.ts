import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';
import { IsUnique } from '../../../resources/utils/validators/is-unique.validator';
import { User } from '../user.entity';

export class CreateUserDto {
  @IsEmail()
  @IsNotEmpty()
  @MinLength(3)
  @ApiProperty()
  @IsUnique('email', User)
  email: string;

  @IsString()
  @ApiProperty()
  @IsNotEmpty()
  fullname: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  @MinLength(6)
  password: string;
}
